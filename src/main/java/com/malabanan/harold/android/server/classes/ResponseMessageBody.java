package com.malabanan.harold.android.server.classes;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class ResponseMessageBody {
    private String message;


    public ResponseMessageBody(String message) {
        this.message = message;
    }

    public ResponseMessageBody(){

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
