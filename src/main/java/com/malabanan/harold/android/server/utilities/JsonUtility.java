package com.malabanan.harold.android.server.utilities;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class JsonUtility {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static String convertToJson(Object object){
        String jsonData = "";
        try {
            jsonData = MAPPER.writeValueAsString(object);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jsonData;
    }

    public static Object convertFromJson(String jsonData, Class className ){
        Object object = null;

        try {
            object = MAPPER.readValue(jsonData,className);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return object;
    }
}
