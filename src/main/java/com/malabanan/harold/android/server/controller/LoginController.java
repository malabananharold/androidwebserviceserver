package com.malabanan.harold.android.server.controller;

import com.malabanan.harold.android.server.classes.Login;
import com.malabanan.harold.android.server.classes.LoginResponse;
import com.malabanan.harold.android.server.classes.ResponseMessage;
import com.malabanan.harold.android.server.classes.ResponseMessageBody;
import com.malabanan.harold.android.server.utilities.DateUtility;
import com.malabanan.harold.android.server.utilities.JsonUtility;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * Created by hmalabanan on 9/13/14.
 */
@Controller
@RequestMapping("/")
public class LoginController {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @RequestMapping(value = "welcome",method = RequestMethod.POST)
    public @ResponseBody ResponseMessage welcome(@RequestBody String userData){
        ResponseMessage responseMessage = new ResponseMessage();
        LoginResponse loginResponse = new LoginResponse();
        Login login = (Login) JsonUtility.convertFromJson(userData,Login.class);

        if("harold".equals(login.getUserName()) && "12345".equals(login.getPassword())){
            responseMessage.setResponseCode(200);
            loginResponse.setUserName(login.getUserName());
            loginResponse.setMessage("Successfully logged in");
            loginResponse.setLoginTime(DateUtility.convertToMMDDYYYFormat());
            responseMessage.setReponseBody(loginResponse);
        }else{
            responseMessage.setResponseCode(200);
            responseMessage.setReponseBody(new ResponseMessageBody("Invalid Credentials"));
        }
        return responseMessage;
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public @ResponseBody String login(){
        return "";
    }
}
