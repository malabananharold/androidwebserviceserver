package com.malabanan.harold.android.server.classes;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class Comments {
    private String userName;
    private Date dateCreated;
    private String commentBody;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public Date getDateCreated() {
        return dateCreated;
    }

    @JsonSerialize(using = JsonDateSerializer.class)
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }
}
