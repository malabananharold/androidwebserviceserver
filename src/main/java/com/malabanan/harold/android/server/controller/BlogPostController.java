package com.malabanan.harold.android.server.controller;

import com.malabanan.harold.android.server.classes.BlogPost;
import com.malabanan.harold.android.server.classes.Comments;
import com.malabanan.harold.android.server.classes.ResponseMessage;
import com.malabanan.harold.android.server.utilities.DateUtility;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hmalabanan on 9/13/14.
 */
@Controller
@RequestMapping("/home")
public class BlogPostController {

    @ResponseBody @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ResponseMessage viewDashBoard(){
        ResponseMessage responseMessage = new ResponseMessage();
        BlogPost blogPost = new BlogPost();


        blogPost.setAuthor("Harold Malabanan");
        blogPost.setDateCreated(new Date());
        blogPost.setTitle("Sample Title");
        blogPost.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque hendrerit quis nibh posuere accumsan. Integer lectus odio, consectetur eu lacus et, facilisis fermentum dui. Curabitur mi ante, tincidunt eget nunc ac, ultricies volutpat velit. Fusce eleifend enim vel ultricies mollis. Mauris quis purus nunc. Cras scelerisque enim nisi, volutpat malesuada mauris hendrerit a. Pellentesque lacinia pretium neque ut malesuada. Vestibulum vitae nunc vestibulum, ultrices ligula vitae, lobortis arcu. Suspendisse potenti. Praesent pharetra viverra sapien nec laoreet. In euismod velit urna, id laoreet risus interdum in. Donec sagittis auctor erat ut malesuada. Mauris non est in risus posuere euismod a sit amet magna. Morbi rutrum, felis vitae mattis tempor, risus odio pretium nisl, in tincidunt velit sem mattis enim. Aenean sit amet aliquam eros, convallis ultricies sem.\n" +
                "\n" +
                "Nam luctus justo diam, et ultrices leo consectetur in. Aenean sed lorem non nulla vehicula tristique a a lacus. Nullam purus arcu, ultrices eget viverra vitae, euismod in tellus. Quisque aliquet nec magna a auctor. Pellentesque non vulputate enim. Aliquam non gravida est, vel vulputate lacus. In eu nisl nec velit scelerisque mattis a et est. Fusce scelerisque suscipit nunc ac dignissim. Aenean quam mauris, bibendum vel suscipit id, elementum non mi.");
        blogPost.setComments(getComments());

        responseMessage.setResponseCode(200);
        responseMessage.setReponseBody(blogPost);

        return responseMessage;
    }

    private List<Comments> getComments(){

        List<Comments> commentsList = new ArrayList<Comments>();

        for(int i =0; i < 3; i++){
            Comments comments = new Comments();
            comments.setUserName("Kenneth");
            comments.setCommentBody("Testing Comment " + i);
            comments.setDateCreated(new Date());
            commentsList.add(comments);
        }

        return commentsList;
    }
}
