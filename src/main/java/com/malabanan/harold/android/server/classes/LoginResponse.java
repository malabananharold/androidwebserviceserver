package com.malabanan.harold.android.server.classes;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class LoginResponse extends ResponseMessageBody {
    private String loginTime;
    private String userName;

    public LoginResponse(String message){
        super(message);
    }

    public LoginResponse(){
        super();
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
