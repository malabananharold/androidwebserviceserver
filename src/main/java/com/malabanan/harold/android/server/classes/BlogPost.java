package com.malabanan.harold.android.server.classes;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.List;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class BlogPost {
    private String title;
    private String author;
    private Date dateCreated;
    private String content;
    private List<Comments> comments;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @JsonSerialize(using=JsonDateSerializer.class)
    public Date getDateCreated() {
        return dateCreated;
    }

    @JsonSerialize(using=JsonDateSerializer.class)
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }
}
