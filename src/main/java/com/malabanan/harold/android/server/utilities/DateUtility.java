package com.malabanan.harold.android.server.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class DateUtility {

    public static String convertToMMDDYYYFormat(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return sdf.format(date);
    }
}
