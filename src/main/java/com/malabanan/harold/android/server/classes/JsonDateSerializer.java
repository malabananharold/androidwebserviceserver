package com.malabanan.harold.android.server.classes;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.util.ISO8601DateFormat;

import java.io.IOException;
import java.util.Date;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class JsonDateSerializer extends JsonSerializer<Date>{

    private static final ISO8601DateFormat DATEFORMAT = new ISO8601DateFormat();

    @Override
    public void serialize(Date date, JsonGenerator gen, SerializerProvider provider)
            throws IOException {

        String formattedDate = DATEFORMAT.format(date);
        gen.writeString(formattedDate);
    }

}
