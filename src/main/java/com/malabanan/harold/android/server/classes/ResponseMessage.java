package com.malabanan.harold.android.server.classes;

/**
 * Created by hmalabanan on 9/13/14.
 */
public class ResponseMessage {
    private int responseCode;
    private Object reponseBody;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public Object getReponseBody() {
        return reponseBody;
    }

    public void setReponseBody(Object reponseBody) {
        this.reponseBody = reponseBody;
    }
}
